class TasksController < ApplicationController

  before_action :find_project_by_id, :only => [:new, :create, :destroy, :toggle_status]
  before_action :find_task_by_id, :only => [:destroy, :toggle_status]

  def new
    @task = @project.tasks.new
  end

  def create
    @task = @project.tasks.new(task_params)
    if @task.save
      redirect_to project_path(@project)
    else
      render :new
    end
  end

  def destroy
    @task.destroy
    redirect_to project_path(@project)
  end

  def toggle_status
    @task.status = @task.status == 0 ? 1 : 0
    if @task.save
      render json: @task.to_json
    else
      render nothing: true, status: 400
    end
  end

  private

  def task_params
    params.require(:task).permit(:title)
  end

  def find_project_by_id
    @project = Project.where(:id => params[:project_id]).first
  end

  def find_task_by_id
    @task = @project.tasks.where(:id => params[:id]).first
  end
end
