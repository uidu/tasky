class ProjectsController < ApplicationController
  before_action :authenticate_user!

  # Defines acceptable MIME-types
  respond_to :html, :json, :only => [:index, :show]

  # Call a function before executing a controler's method
  before_action :find_project_by_id, :only => [:show, :edit, :update, :destroy]

  def index
    @projects = current_user.projects
    respond_with @projects
  end

  def new
    @project = Project.new
  end

  def create
    @project = current_user.projects.new(project_params)
    if @project.save
      redirect_to projects_path
    else
      render :new
    end
  end

  def show
    # @project set by filter
    @tasks = @project.tasks
  end

  def edit
    # @project set by filter
  end

  def update
    # @project set by filter

    if @project.update_attributes(project_params)
      redirect_to project_path(@project)
    else
      render :edit
    end
  end

  def destroy
    # @project set by filter

    @project.destroy
    redirect_to projects_path
  end

  private

  def find_project_by_id
    @project = Project.where(:id => params[:id]).first
  end

  def project_params
    params.require(:project).permit(:title, :description, :cover)
  end
end
