class Project < ActiveRecord::Base
  mount_uploader :cover, CoverUploader

  has_many :tasks
  belongs_to :user
  validates :title, :presence => true
end
